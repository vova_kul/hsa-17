# HSA-17 Homework project for algorithm and data structures

This an HSA-17 Homework project to show efficency of Balanced Binary Search Tree and Counting Sort algorithm.

## Balanced Binary Search Tree

#### Run

```
python red_black_tree_benchmark.py
```

#### Result inserts

```
Inserting 1000 elements into a tree took 11.2090110778808 milliseconds
Inserting 10000 elements into a tree took 139.8780345916748 milliseconds
Inserting 100000 elements into a tree took 2056.8301677703857 milliseconds
Inserting 1000000 elements into a tree took 29200.14214515686 milliseconds
Inserting 2000000 elements into a tree took 62101.922035217285 milliseconds
Inserting 5000000 elements into a tree took 163867.99693107605 milliseconds
```

![Inserts](./screens/Inserts.png?raw=true "Inserts")

Note: ratio shows that insert complexity per element in bigger datasets grows in logarithmic time

#### Result search

```
Searching an element in a tree with size 1000 took 0.1499652862548828 milliseconds
Searching an element in a tree with size 10000 took 0.24819374084472656 milliseconds
Searching an element in a tree with size 100000 took 0.2181529998779297 milliseconds
Searching an element in a tree with size 1000000 took 0.2932548522949215 milliseconds
Searching an element in a tree with size 2000000 took 0.3991127014160156 milliseconds
Searching an element in a tree with size 5000000 took 0.04410743713378906 milliseconds
```

![Search](./screens/Search.png?raw=true "Search")

#### Result deletes

```
Deleting an element in a tree with size 1000 took 0.1800060272216797 milliseconds
Deleting an element in a tree with size 10000 took 0.2622604370117632 milliseconds
Deleting an element in a tree with size 100000 took 0.2892017364501953 milliseconds
Deleting an element in a tree with size 1000000 took 0.17714500427246094 milliseconds
Deleting an element in a tree with size 2000000 took 0.1270771026611328 milliseconds
Deleting an element in a tree with size 5000000 took 0.06890296936035156 milliseconds
```

![Deletes](./screens/Deletes.png?raw=true "Deletes")

## Counting Sort algorithm

#### Run
```
python counting_sort.py
```
#### Result

```
Generating random array of length 50 000 000 and max value 50 000 000 (small varience)
Start sorting
Sorting finished in 85.18424797058105 seconds
Generating random array of length 50 000 000 and max value 2 500 000 000 000 000 (large varience)
Start sorting
Sorting finished in 89.20822191238403 seconds
```

#### Conclusion
Counting Sort doesn’t perform when the value of the largest element k exceeds the number of elements in the input array n.