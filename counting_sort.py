import random
import time

def counting_sort(input_array):
    # Find the maximum element in the input array
    max_element= max(input_array)

    count_array_length = max_element+1

    # Initialize the count_array with (max+1) zeros
    count_array = [0] * count_array_length

    # Step 1 -> Traverse the inputArray and increase 
    # the corresponding count for every element by 1
    for el in input_array: 
        count_array[el] += 1

    # Step 2 -> For each element in the count_array, 
    # sum up its value with the value of the previous 
    # element, and then store that value 
    # as the value of the current element
    for i in range(1, count_array_length):
        count_array[i] += count_array[i-1] 

    # Step 3 -> Calculate element position
    # based on the count_array values
    outputArray = [0] * len(input_array)
    i = len(input_array) - 1
    while i >= 0:
        currentEl = input_array[i]
        count_array[currentEl] -= 1
        newPosition = count_array[currentEl]
        outputArray[newPosition] = currentEl
        i -= 1

    return outputArray


def generate_random_array(length, max_value):
    return [random.randint(0, max_value) for _ in range(length)]

if __name__ == "__main__":
    print("Generating random array of length 50 000 000 and max value 50 000 000 (small varience)")
    random_array = generate_random_array(50_000_000, 50_000_000)
    print("Start sorting")
    start_time = time.time()
    counting_sort(random_array)
    end_time = time.time()
    print("Sorting finished in {} seconds".format(end_time-start_time))
    
    print("Generating random array of length 50 000 000 and max value 2 500 000 000 000 000 (large varience)")
    random_array_with_big_varience = generate_random_array(50_000_000, 2_500_000_000_000_000)
    print("Start sorting")
    start_time = time.time()
    counting_sort(random_array)
    end_time = time.time()
    print("Sorting finished in {} seconds".format(end_time-start_time))