import random

from red_black_tree import RedBlackTree

import time

BENCHMARKS_LEVELS = [1000, 10_000, 100_000, 1_000_000, 2_000_000, 5_000_000]

def generate_random_array(length):
    random_array = [random.randint(0, length) for _ in range(length - 1)]
    random_array.append(25) # test value to search and delete
    return random_array

def generate_random_arrays_for_benchmarks(benchmarks_levels):
    return {length: generate_random_array(length) for length in benchmarks_levels.keys()}

def insert_array_into_tree(tree, array):
    for element in array:
        tree.insert(element)
    return tree

def find_node(tree, element):
    return tree.search(element)

def remove_node(tree, node):
    tree.delete(node)

def benchmark_insert(benchmarks_levels):
    benchmarks = generate_random_arrays_for_benchmarks(benchmarks_levels)

    for length, tree in benchmarks_levels.items():
        array = benchmarks[length]
        start_time = time.time()
        insert_array_into_tree(tree, array)
        end_time = time.time()
        elapsed_time_in_milliseconds = (end_time-start_time) * 1000
        print("Inserting {} elements into a tree took {} milliseconds".format(len(array), elapsed_time_in_milliseconds))

def benchmark_search(benchmarks_levels):
    for length, tree in benchmarks_levels.items():
        start_time = time.time()
        find_node(tree, 25)
        end_time = time.time()
        elapsed_time_in_milliseconds = (end_time-start_time) * 1000
        print("Searching an element in a tree with size {} took {} milliseconds".format(length, elapsed_time_in_milliseconds))

def benchmark_delete(benchmarks_levels):
    for length, tree in benchmarks_levels.items():
        start_time = time.time()
        remove_node(tree, 25)
        end_time = time.time()
        elapsed_time_in_milliseconds = (end_time-start_time) * 1000
        print("Deleting an element in a tree with size {} took {} milliseconds".format(length, elapsed_time_in_milliseconds))

if __name__ == "__main__":
    benchmark_levels_with_trees = {benchmark_level: RedBlackTree() for benchmark_level in BENCHMARKS_LEVELS}
    benchmark_insert(benchmark_levels_with_trees)
    benchmark_search(benchmark_levels_with_trees)
    benchmark_delete(benchmark_levels_with_trees)
